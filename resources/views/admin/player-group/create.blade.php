@extends('admin.layouts.dashboard')
@section('content')
    <h2>Player Group</h2>
    <form action="{{route('admin.player-group.store')}}" method="post">
    	@csrf
    	<div class="row">
			<div class="col-md-6">
			    <label>Name</label>
			    <input type="text" name="name">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			    <label>Team Name</label>
			    <select name="team_id">
			    	<option value="">Please Select Team</option>
			    	@foreach($teams as $team)
			    		<option value="{{$team->id}}">{{$team->name}}</option>
			    	@endforeach
				</select>
			</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Create" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection