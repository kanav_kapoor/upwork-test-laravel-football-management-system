@extends('admin.layouts.dashboard')
@section('content')
    <h2>Team</h2>
    <form action="{{route('admin.team.store')}}" method="post">
    	@csrf
    	<div class="row">
			<div class="col-md-6">
			    <label>Name</label>
			    <input type="text" name="name">
			</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Create" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection