@extends('admin.layouts.dashboard')
@section('content')
    <h2>Team</h2>
    <form action="{{route('admin.team.update', $team->id)}}" method="post">
    	@csrf
    	@method('PATCH')
    	<div class="row">
			<div class="col-md-6">
			    <label>Name</label>
			    <input type="text" name="name" value="{{$team->name}}">
			</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Update" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection