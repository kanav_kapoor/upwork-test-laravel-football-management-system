@extends('admin.layouts.dashboard')
@section('content')
    <h2>Club</h2>
    <form action="{{route('admin.club.store')}}" method="post">
    	@include('admin.club._fields')
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Create" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection