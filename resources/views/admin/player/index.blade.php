@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
        <a href="{{route('admin.player.create')}}" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new player</a>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Player Group</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($players as $player)
        <tr>
            <td>{{$player->id}}</td>
            <td>{{$player->name}}</td>
            <td>{{$player->name}}</td>
            <td class="text-center"><a class='btn btn-info btn-xs' href="{{route('admin.player.edit', $player->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{route('admin.player.destroy', $player->id)}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
        </tr>
        @endforeach
    </table>
@endsection