<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = ['name'];

    public function admin() 
    {
    	return $this->hasOne('App\User');
    }
}
