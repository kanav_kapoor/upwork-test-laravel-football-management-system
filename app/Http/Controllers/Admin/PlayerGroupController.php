<?php

namespace App\Http\Controllers\Admin;

use App\Team;
use App\PlayerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlayerGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $playerGroups = PlayerGroup::all();
        return view('admin.player-group.index', compact('playerGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        return view('admin.player-group.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
                    'name'    => 'required|max:255|min:2',
                    'team_id' => 'required',
                ]);
        $playerGroup = PlayerGroup::create($data);

        return redirect()->route('admin.player-group.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PlayerGroup $playerGroup)
    {
        return view('admin.player-group.create', compact('playerGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
                    'name'    => 'required|max:255|min:2',
                    'team_id' => 'required',
                ]);
        $playerGroup->update($data);

        return redirect()->route('admin.player-group.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlayerGroup $playerGroup)
    {
        dd($playerGroup); 
    }
}
