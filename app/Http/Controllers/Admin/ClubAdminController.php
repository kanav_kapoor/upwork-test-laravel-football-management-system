<?php

namespace App\Http\Controllers\Admin;

use App\Club;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClubAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubAdmin = User::where('club_id', '!=', '')->get();
        return view('admin.club-admin.index', compact('clubAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Club $club)
    {
        return view('admin.club-admin.create', compact('club'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Club $club)
    {
        $data = $request->validate([
            'name' => 'required|max:255|min:2',
            'email' => 'required|email',
        ]);

        $clubAdmin = $club->admin()->create($data);

        return redirect()->route('admin.club.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $clubAdmin)
    {
        return view('admin.club-admin.edit', compact('clubAdmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $clubAdmin)
    {
        $data = $request->validate([
            'name' => 'required|max:255|min:2',
        ]);

        $clubAdmin->update($data);

        return redirect()->route('admin.club.admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $clubAdmin)
    {
        dd($clubAdmin);
    }
}
